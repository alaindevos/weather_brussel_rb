#!/usr/bin/env /usr/local/bin/ruby
#Copyright (C) 2016 Alain De Vos <devosalain@ymail.com>
mywoeid="968019"
['net/http','json'].each {|file| require file }
def mydata(res) 
	return res["date"]+" "+res["day"]+" "+res["high"]+" "+res["low"]+" "+res["text"] 
end
myurl=%Q[http://query.yahooapis.com/v1/public/yql?q=SELECT%20*%20FROM%20weather.forecast%20WHERE%20woeid="#{mywoeid}"%20and%20u="c"&format=json]
res = JSON.parse(Net::HTTP.get(URI.parse(myurl)))["query"]["results"]["channel"]["item"]["forecast"]
(0..4).each { |x| print mydata(res[x])+"\n" }
